# Oak-Wordpress Boilerplate Image

This is the docker image made specifically for the web bureau [OakDigital](https://www.oakdigital.dk).

## What is included

This image is a heavily modified version of the official Wordpress fpm-alpine image. It is made to be run locally in the Oak docker stack, for local development (it is not meant for production, but could probably be used for production with a few additions).

It includes following features:

 - Alpine Linux
 - PHP FPM
 - WP-CLI
 - NPM
 - Composer

## Setup

The provided `docker-compose` file is setup for use with the [caddy-docker-proxy](https://github.com/lucaslorentz/caddy-docker-proxy) image by lucaslorentz, and the provided `.env` file has an example for environment variables for setup.

Run `docker-compose` with `--env-file=path/to/.env` to populate the variables in the compose file.

## Build Args

 - `USER_ID`: (default: 1000) The ID of you local user, typically 1000, this is used to have the correct permissions.
 - `USER_NAME`: (default: docker The name of the user in the container.

## Environment Variables

 - `SERVICE_NAME`: The name of the service, this will both be used as container name and as default the name of the domain and source folder (It is important the source folder is named the same thing in the container as it will be in the caddy volume).
 - `SERVICE_DOMAIN`: (default: `$SERVICE_NAME.localhost`) The domain
 - `SERVICE_ROOT_DIR`: (default: `$SERVICE_NAME`) The root directory name of the service, in the container it is relative to `/var/www/`.
 - `POST_INSTALL_CMD`: (default: NULL) A command to execute after the entry script is finished, good for additional setup.
 - `WORDPRESS_DB_HOST`: database host
 - `WORDPRESS_DB_USER`: database user
 - `WORDPRESS_DB_PASSWORD`: database password
 - `WORDPRESS_DB_NAME`: Wordpress database name
 - `WORDPRESS_TABLE_PREFIX`: Wordpress database prefix
 - `WORDPRESS_DEBUG`: Debug mode for wordpress
 - `WORDPRESS_URL`: Wordpress page url (should be the same as SERVICE_DOMAIN)
 - `WORDPRESS_TITLE`: The title of the wordpress page
 - `WORDPRESS_ADMIN_EMAIL`: Mail for the wordpress admin
 - `WORDPRESS_ADMIN_USERNAME`: Username for the wordpress admin
 - `WORDPRESS_ADMIN_PASSWORD`: Password for the wordpress admin

## Tags

As of right now `:beta` is the only tag.

## TODO:

 - Cleanup of entrypoint script
 - Better handling of env variables
 - Better readme
 - PHP versions
 - Windows build for native windows docker (no promises)
